import pygame
import random


# basic colors
black = (0, 0, 0)
white = (255, 255, 255)
red = (255, 0, 0)
green = (0, 255, 0)
blue = (0, 0, 255)


class Background:

    image_fnames = ['images/background_1.png', 'images/background_2.png',
                    'images/background_3.png', 'images/background_4.png',
                    'images/background_5.png', 'images/background_6.png',
                    'images/background_7.png', 'images/background_8.png',
                    'images/background_9.png', 'images/background_10.png',
                    ]

    def __init__(self, location=(0, 0)):
        self.setup_images()
        self.draw_count = 0
        self.rect.left, self.rect.top = location

    def setup_images(self):
        images = []
        for fname in self.image_fnames:
            image = pygame.image.load(fname)
            image = pygame.transform.scale(image, (1600, 1000))
            images.append(image)
        self.images = images
        self.image = self.images[0]
        self.rect = self.image.get_rect()

    def draw(self, screen):
        self.draw_count += 1
        if self.draw_count > 25:
            self.draw_count = 0
            self.image = random.choice(self.images)
        screen.blit(self.image, self.rect)


class Text:

    def __init__(self, text_str, position, fontsize=60, fontstyle='comicsans', color=black):
        self.x, self.y = position
        self.fontsize = fontsize
        self.fontstyle = fontstyle
        self.color = color
        self.set_text_str(text_str)

    def set_text_str(self, new_str):
        self.text_str = new_str
        font = pygame.font.SysFont(self.fontstyle, self.fontsize)
        self.text = font.render(self.text_str, 1, self.color)

    def draw(self, screen):
        screen.blit(self.text, (self.x, self.y))

    def set_centerx(self, centerx):
        self.x = centerx - self.text.get_width()/2

    def set_centery(self, centery):
        self.y = centery - self.text.get_height()/2

    def set_center(self, center):
        self.set_centerx(center[0])
        self.set_centery(center[1])


class Button:

    background_fill = (255, 255, 255, 200)

    def __init__(self, location, width, height, text_str='', outline_width=4, fontsize=50):

        self.fontsize = fontsize
        self.x, self.y = location
        self.width = width
        self.height = height
        self.text_str = text_str

        # setup button box
        self.rect = pygame.rect.Rect(self.x, self.y, self.width, self.height)

        # setup text
        self.text = Text(text_str, (0, 0), fontsize=self.fontsize)
        self.text.set_center(self.rect.center)

    def draw(self, screen):

        # box
        s = pygame.Surface((self.rect.width, self.rect.height), pygame.SRCALPHA)
        s.fill(self.background_fill)
        screen.blit(s, self.rect.topleft)

        # text
        self.text.draw(screen)

    def is_over(self, pos):
        """Pos is the mouse position or a tuple of (x,y) coordinates"""
        is_over = self.rect.collidepoint(*pos)
        return is_over
