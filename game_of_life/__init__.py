from .game_of_life import GameOfLife
from .continous_life import ContinousLife
from .color_life import ColorLife
from .cellular_automata import CellularAutomata