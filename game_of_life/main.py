import pygame
from states import MainMenuState, ExitState, State


# Constants
size_x = 1600
size_y = 1000
FPS = 20

# setup display
pygame.init()
screen = pygame.display.set_mode((size_x, size_y), pygame.SRCALPHA, 32)
constants = dict(size_x=size_x, size_y=size_y, FPS=FPS, screen=screen)


# main loop
state = MainMenuState(constants)
while True:
    if not isinstance(state, State):
        print('Found game in bad state,', state, ', exiting ...')
        state = ExitState()

    print('DEBUG: current state', state)
    next_state = state()
    print('DEBUG: next state', next_state)

    state = next_state

exit = ExitState()
exit()
