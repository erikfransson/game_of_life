import numpy as np
import pygame
from game_of_life import GameOfLife


colors1 = [
    (247, 252, 253),
    (229, 245, 249),
    (204, 236, 230),
    (153, 216, 201),
    (102, 194, 164),
    (65, 174, 118),
    (35, 139, 69),
    (0, 109, 44),
    (0, 68, 27),
]


colors2 = [
    (247, 252, 253),
    (224, 236, 244),
    (191, 211, 230),
    (158, 188, 218),
    (140, 150, 198),
    (140, 107, 177),
    (136, 65, 157),
    (129, 15, 124),
    (77, 0, 75),
]

colors3 = [
    (247, 252, 240),
    (224, 243, 219),
    (204, 235, 197),
    (168, 221, 181),
    (123, 204, 196),
    (78, 179, 211),
    (43, 140, 190),
    (8, 104, 172),
    (8, 64, 129),
]

colors4 = [
    (255, 247, 236),
    (254, 232, 200),
    (253, 212, 158),
    (253, 187, 132),
    (252, 141, 89),
    (239, 101, 72),
    (215, 48, 31),
    (179, 0, 0),
    (127, 0, 0),
]

colors5 = [
    (255, 247, 251),
    (236, 231, 242),
    (208, 209, 230),
    (166, 189, 219),
    (116, 169, 207),
    (54, 144, 192),
    (5, 112, 176),
    (4, 90, 141),
    (2, 56, 88),
]

colors6 = [
    (255, 247, 251),
    (236, 226, 240),
    (208, 209, 230),
    (166, 189, 219),
    (103, 169, 207),
    (54, 144, 192),
    (2, 129, 138),
    (1, 108, 89),
    (1, 70, 54),
]

colors7 = [
    (247, 244, 249),
    (231, 225, 239),
    (212, 185, 218),
    (201, 148, 199),
    (223, 101, 176),
    (231, 41, 138),
    (206, 18, 86),
    (152, 0, 67),
    (103, 0, 31),
]

colors8 = [
    (255, 247, 243),
    (253, 224, 221),
    (252, 197, 192),
    (250, 159, 181),
    (247, 104, 161),
    (221, 52, 151),
    (174, 1, 126),
    (122, 1, 119),
    (73, 0, 106),
]


colors9 = [
    (255, 255, 229),
    (247, 252, 185),
    (217, 240, 163),
    (173, 221, 142),
    (120, 198, 121),
    (65, 171, 93),
    (35, 132, 67),
    (0, 104, 55),
    (0, 69, 41),
]

colors10 = [
    (255, 255, 217),
    (237, 248, 177),
    (199, 233, 180),
    (127, 205, 187),
    (65, 182, 196),
    (29, 145, 192),
    (34, 94, 168),
    (37, 52, 148),
    (8, 29, 88),
]

colors11 = [
    (255, 255, 229),
    (255, 247, 188),
    (254, 227, 145),
    (254, 196, 79),
    (254, 153, 41),
    (236, 112, 20),
    (204, 76, 2),
    (153, 52, 4),
    (102, 37, 6),
]

colors12 = [
    (255, 255, 204),
    (255, 237, 160),
    (254, 217, 118),
    (254, 178, 76),
    (253, 141, 60),
    (252, 78, 42),
    (227, 26, 28),
    (189, 0, 38),
    (128, 0, 38),
]

colors13 = [
    (228, 26, 28),
    (55, 126, 184),
    (77, 175, 74),
    (152, 78, 163),
    (255, 127, 0),
    (255, 255, 51),
    (166, 86, 40),
    (247, 129, 191),
    (153, 153, 153),
]


class ColorGradients:
    """
    Check
    https://colorbrewer2.org/#type=sequential&scheme=YlOrRd&n=9
    """

    color_gradients = [colors1, colors2, colors3, colors4, colors5, colors6, colors7,
                       colors8, colors9, colors10, colors11, colors12, colors13]

    @classmethod
    def get_pygame_colors(cls, i):
        colors = cls.color_gradients[i]
        return {k: pygame.Color(*c) for k, c in enumerate(reversed(colors))}

    @classmethod
    def plot_color_gradients(cls):
        pass


class ColorLife(GameOfLife):

    def __init__(self, color_index=0, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.color_gradient = ColorGradients.get_pygame_colors(color_index)

    def draw(self, screen, show_step=False):

        screen.fill(self.background_color)

        for x in range(self.size_x):
            for y in range(self.size_y):
                # color = self.alive_color if self.board[(x, y)] == 1 else self.dead_color

                if self.board[(x, y)] == 1:
                    nbrs = self.nbr_list[(x, y)]
                    nbrs_alive = sum(self.board[n] for n in nbrs)
                    color = self.color_gradient[nbrs_alive]
                else:
                    color = self.dead_color

                r = self.rects[(x, y)]
                pygame.draw.rect(screen, color, r, 0)

        # draw text
        if show_step:
            text_str = 'step {}'.format(self.step)
            font = pygame.font.SysFont('Times', 80)
            self.text = font.render(text_str, 1, (255, 255, 255))
            screen.blit(self.text, (50, 30))


if __name__ == '__main__':

    # setup
    pygame.init()
    np.random.seed(200)

    # Constants
    size_x = 1600
    size_y = 900
    FPS = 10

    # setup display
    pygame.init()
    screen = pygame.display.set_mode((size_x, size_y), pygame.SRCALPHA, 32)

    # grid size
    cell_size = 10
    color_index = 1
    size_xs = size_x / cell_size
    size_ys = size_y / cell_size

    # assert grid perfectly fits into canvas
    assert abs(size_xs - int(size_xs)) < 1e-10
    assert abs(size_ys - int(size_ys)) < 1e-10
    size_xs = int(size_xs)
    size_ys = int(size_ys)

    # game rules
    B = [3, 4]
    S = [2, 3]
    nh = 'Moore'
    starting_density = 0.05

    # run game of life
    game = ColorLife(size_x=size_xs, size_y=size_ys, B=B, S=S, nh=nh, color_index=color_index,
                     cell_size=cell_size, starting_density=starting_density)
    game.run(screen, FPS=FPS)
    pygame.quit()
