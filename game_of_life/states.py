import sys
import pygame
from gui import Background, Button
from game_of_life import GameOfLife
from japan_world import JapanGameOfLife


def get_conoway_kwargs():
    kwargs = dict(B=[3], S=[2, 3], nh='Moore', starting_density=0.5)
    return kwargs


class State:
    """ Basic game state """

    def __init__(self, constants):
        self._constants = constants
        self.size_x = constants['size_x']
        self.size_y = constants['size_y']
        self.FPS = constants['FPS']
        self.screen = constants['screen']

    def __call__(self):
        """ Run state """
        pass

    def __repr__(self):
        return self.__class__.__name__


class ExitState(State):

    def __init__(self):
        pass

    def __call__(self):
        pygame.quit()
        sys.exit()


class MainMenuState(State):

    def __call__(self):

        # button constants
        b_width = 350
        b_height = 80
        x1 = int(0.5 * (self.size_x - b_width)/2)
        x2 = int(1.5 * (self.size_x - b_width)/2)
        x3 = int((x1+x2)/2)
        dy = 150
        y1 = 100
        y2 = y1 + 1 * dy
        y3 = y1 + 2 * dy
        y4 = y1 + 3 * dy
        y5 = y1 + 4 * dy
        y6 = y1 + 5 * dy

        # setup
        background = Background()
        clock = pygame.time.Clock()

        # setup buttons
        gol_buttons = [
            Button((x1, y1), b_width, b_height, "Conway's Game of Life", fontsize=40),
            Button((x2, y1), b_width, b_height, 'Moore - Neumann', fontsize=40),
            Button((x1, y2), b_width, b_height, 'Seeds and spaceships', fontsize=40),
            Button((x2, y2), b_width, b_height, 'Star wars', fontsize=40),
            Button((x1, y3), b_width, b_height, 'Outside order', fontsize=40),
            Button((x2, y3), b_width, b_height, 'Alien symbols', fontsize=40),
            Button((x1, y4), b_width, b_height, 'Grain growth', fontsize=40),
            Button((x2, y4), b_width, b_height, 'Walled cities', fontsize=40),
            Button((x1, y5), b_width, b_height, 'Maze builder', fontsize=40),
            ]
        gol_kwargs = [
            dict(B=[3], S=[2, 3], scale=8, nh='Moore', starting_density=0.5),
            dict(B=[2, 4], S=[2, 3], scale=6, nh='Neumann_r', starting_density=0.75),
            dict(B=[2], S=[], scale=6, nh='Moore', starting_density=0.01),
            dict(B=[2, 3, 4, 5], S=[4, 5, 6, 7, 8], scale=6, nh='Moore', starting_density=0.01),
            dict(B=[4, 5], S=[1, 2, 3, 4, 5], scale=6, nh='Moore', starting_density=0.35),
            dict(B=[3, 4], S=[2, 3], scale=4, nh='Moore', starting_density=0.05),
            dict(B=[4, 5, 6, 7, 8], S=[2, 3, 4, 5, 6, 7, 8], scale=4, nh='Moore', starting_density=0.2),
            dict(B=[4, 5, 6, 7, 8], S=[2, 3, 4, 5], scale=4, nh='Moore', starting_density=0.2),
            dict(B=[3], S=[2, 3, 4, 5], scale=6, nh='Moore', starting_density=0.03),
            ]

        japan_button = Button((x2, y5), b_width, b_height, 'Neumann in Japan', fontsize=40)
        exit_button = Button((x3, y6), b_width, b_height, 'Exit', fontsize=60)

        while True:

            # draw menu
            background.draw(self.screen)
            for b in gol_buttons:
                b.draw(self.screen)
            japan_button.draw(self.screen)
            exit_button.draw(self.screen)

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    return ExitState()

                # handle clicking
                if event.type == pygame.MOUSEBUTTONUP:
                    mouse_position = pygame.mouse.get_pos()

                    for b, kwargs in zip(gol_buttons, gol_kwargs):
                        if b.is_over(mouse_position):
                            return GOLState(self._constants, **kwargs)

                    if japan_button.is_over(mouse_position):
                        return JapanState(self._constants)

                    if exit_button.is_over(mouse_position):
                        return ExitState()

            # draw main menu
            pygame.display.flip()
            clock.tick(self.FPS)


class GOLState(State):

    def __init__(self, constants, scale, **kwargs):
        super().__init__(constants)
        self.scale = scale
        self.kwargs = kwargs
        self.kwargs['size_x'] = self.size_x // (self.scale)
        self.kwargs['size_y'] = self.size_y // (self.scale)
        self.kwargs['cell_size'] = self.scale
        self.screen.fill((0, 0, 0))
        pygame.display.flip()

    def __call__(self):
        gol = GameOfLife(**self.kwargs)
        gol.run(self.screen, self.FPS)
        return MainMenuState(self._constants)


class JapanState(GOLState):

    def __init__(self, constants):
        kwargs = dict(scale=5)
        kwargs['B'] = [2, 4]
        kwargs['S'] = [2, 3]
        kwargs['starting_density'] = 0.7
        super().__init__(constants, **kwargs)

    def __call__(self):
        gol = JapanGameOfLife(**self.kwargs)
        gol.run(self.screen, self.FPS)
        return MainMenuState(self._constants)
