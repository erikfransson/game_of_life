import pygame
import numpy as np
from game_of_life import GameOfLife


class JapanGameOfLife(GameOfLife):

    colors = {0: pygame.Color(200, 50, 50),
              1: pygame.Color(60, 125, 216)}

    def generate_neighbor_list(self, nh):
        self._generate_japan()

    def _generate_japan(self):
        sides = [[-1, 0], [1, 0], [0, -1], [0, 1]]
        corners = [[1, 1], [1, -1], [-1, 1], [-1, -1]]
        dirs_lookup = {0: corners, 1: sides}

        r = int(0.9 * self.size_y/2)
        x0 = int(self.size_x/2)
        y0 = int(self.size_y/2)

        self.types = dict()
        nbr_list = dict()
        for x in range(self.size_x):
            for y in range(self.size_y):
                t = int((x-x0)**2 + (y-y0)**2 > r**2)
                self.types[x, y] = t
                dirs = dirs_lookup[t]
                nbrs = []
                for n, m in dirs:
                    if self._inside_board(x+n, y+m):
                        nbrs.append((x+n, y+m))
                nbr_list[(x, y)] = nbrs
        self.nbr_list = nbr_list

    def draw(self, screen, show_step):

        screen.fill(self.background_color)

        for x in range(self.size_x):
            for y in range(self.size_y):
                if self.board[x, y] == 1:
                    color = self.colors[self.types[x, y]]
                else:
                    color = self.dead_color

                r = self.rects[(x, y)]
                pygame.draw.rect(screen, color, r, 0)

        # draw text
        if show_step:
            text_str = 'step {}'.format(self.step)
            font = pygame.font.SysFont('Times', 80)
            self.text = font.render(text_str, 1, (255, 255, 255))
            screen.blit(self.text, (50, 30))


if __name__ == '__main__':

    # setup
    pygame.init()
    np.random.seed(192)

    # Constants
    size_x = 1600
    size_y = 900
    FPS = 20

    # setup display
    pygame.init()
    screen = pygame.display.set_mode((size_x, size_y), pygame.SRCALPHA, 32)

    # grid size
    cell_size = 4
    size_xs = size_x / cell_size
    size_ys = size_y / cell_size

    # assert grid perfectly fits into canvas
    assert abs(size_xs - int(size_xs)) < 1e-10
    assert abs(size_ys - int(size_ys)) < 1e-10
    size_xs = int(size_xs)
    size_ys = int(size_ys)

    # Select rules
    kwargs = dict()
    kwargs['size_x'] = size_xs
    kwargs['size_y'] = size_ys
    kwargs['B'] = [2, 4]
    kwargs['S'] = [2, 3]
    kwargs['starting_density'] = 0.5
    kwargs['cell_size'] = cell_size

    game = JapanGameOfLife(**kwargs)
    game.setup_grid(margin=0.0)
    game.run(screen, FPS=FPS)
    pygame.exit()
