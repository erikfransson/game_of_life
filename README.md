# Game of Life

Cellular automata and game of life on a square 2D lattice. 

Run the GUI via

    python src/main.py

Checkout individual worlds/rules via

    python game_of_life/game_of_life.py
    python game_of_life/japan_world.py
    python game_of_life/cellular_automata.py
    python game_of_life/color_life.py
    python game_of_life/multi_state_life.py
    python game_of_life/continous_life.py


## Birth and death rules
A cell is born if its currently dead and the number of alive neighbors matches the birth numbers, e.g. [2, 3].
A cell stays alive if the number of alive neighbors matches the survival numbers, e.g. [2, 3, 4]

The rules are given can be given as B23/S234.


## Neighbourhoods
Most commonly the neighbors are given by the [Moore neighbourhood](https://en.wikipedia.org/wiki/Moore_neighborhood).    
Another alternative is using the [Neumann neighborhood](https://en.wikipedia.org/wiki/Von_Neumann_neighborhood).

## Interesting worlds
List of interesting rules can be found [here](http://psoup.math.wisc.edu/mcell/rullex_life.html)


#### Moore neighbourhoods
* B34/S23       low starting density yields interesting patterns
* B36/S23       High life  https://en.wikipedia.org/wiki/Highlife_(cellular_automaton)
* B2/S          Seeds   https://en.wikipedia.org/wiki/Seeds_(cellular_automaton)
* B45678/S2345  Walled-cities
* B2345/S45678  Cool exploding when starting from low density
* B45/S12345    Maze like turns into linear

#### Neumann neighbourhoods
* B24/S23  QR-codes


## Explore further
* Multi neighborhood Cellular Automata (MNCA)
* Real valued cellular automata / Continuous automaton
* Multi state CA, e.g. rock-paper-scissors


## TODO:
* Update GUI part to include some CCA, ColorLife, CA
* Rewrite base game of life class to include screen object making simulations more convenient to run



## Demo
![Demo](images/game_of_life.gif)


### Conway's game of life
<img src="images/background_7.png" alt="drawing" width="750"/>



### Variations
<img src="images/background_1.png" alt="drawing" width="750"/>
<img src="images/background_2.png" alt="drawing" width="750"/>
<img src="images/background_3.png" alt="drawing" width="750"/>
<img src="images/background_4.png" alt="drawing" width="750"/>
<img src="images/background_5.png" alt="drawing" width="750"/>
<img src="images/background_6.png" alt="drawing" width="750"/>
<img src="images/background_8.png" alt="drawing" width="750"/>
<img src="images/background_9.png" alt="drawing" width="750"/>
